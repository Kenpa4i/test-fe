import { resolve } from 'path';
import webpack from 'webpack';
import htmlWebpackPlugin from 'html-webpack-plugin';
import { CheckerPlugin } from 'awesome-typescript-loader';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';

const isDevelopment = process.env.NODE_ENV !== 'production';

const htmlWebpackPluginConfig = new htmlWebpackPlugin({
  template: `${__dirname}/src/index.html`,
  filename: 'index.html',
  inject: 'body',
  baseUrl: '/',
});
const config: webpack.Configuration = {
  entry: {
    main: './src/index.tsx',
  },
  output: {
    filename: '[name].[contenthash].bundle.js',
    sourceMapFilename: '[name].[contenthash].bundle.js.map',
    path: resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  devtool: 'eval-source-map',
  mode: isDevelopment ? 'development' : 'production',
  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    symlinks: false,
    cacheWithContext: false,
    extensions: ['.ts', '.tsx', '.js', '.json', '.css'],
    alias: {
      store: resolve(__dirname, 'src/store/'),
      components: resolve(__dirname, 'src/components/'),
      services: resolve(__dirname, 'src/services/'),
      config: resolve(__dirname, 'src/config.ts'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        use: [
          'awesome-typescript-loader',
        ],
      }, {
        test: /.js$/,
        include: resolve(__dirname, 'src/'),
        loader: 'source-map-loader',
        enforce: 'pre',
      }, {
        test: /\.global\.css$/,
        include: [
          resolve(__dirname, 'src/'),
        ],
        use: [
          'style-loader',
          'css-loader',
        ],
      }, {
        test: /^((?!\.global).)*\.css$/,
        use: [
          'style-loader',
          {
            loader: 'typings-for-css-modules-loader',
            options: {
              modules: true,
              importLoaders: 1,
              localIdentName: '[name]__[local]___[hash:base64:5]',
              namedExport: true,
              camelCase: true,
            },
          },
        ],
      },
    ],
  },
  devServer: {
    contentBase: './dist',
    historyApiFallback: true,
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CheckerPlugin(),
    htmlWebpackPluginConfig,
    new webpack.WatchIgnorePlugin([/css\.d\.ts$/]),
  ],
  performance: {
    hints: false,
  },
  optimization: {
    minimize: !isDevelopment,
    flagIncludedChunks: true,
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: false,
        vendor: {
          name: 'vendor',
          chunks: 'all',
          test: /node_modules/,
          priority: 1,
        },
        common: {
          name: 'common',
          minChunks: 2,
          chunks: 'async',
          priority: 10,
          reuseExistingChunk: true,
          enforce: true,
        },
      },
    },
  },
};

export default config;
