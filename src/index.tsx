import { createBrowserHistory } from 'history';
import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { App } from './App';

import store from './store';

const constructedStore = store();
render(
  <Provider store={constructedStore}>
    <App/>
  </Provider>,
  document.getElementById('app'),
);
