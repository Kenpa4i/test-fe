import * as users from './api/users';

export default {
  api: {
    users,
  },
};
