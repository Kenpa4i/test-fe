import axios, { AxiosResponse } from 'axios';
import { Users } from 'store/users/types';
import { API_BASE_URL } from 'config';

export const getUserList = async (): Promise<AxiosResponse<Users.User[]>> =>
  await axios.get(`${API_BASE_URL}/users`);

export const postUser = async (data: Users.UserData): Promise<AxiosResponse<Users.User>> =>
  await axios.post(`${API_BASE_URL}/users/create`, data);

export const deleteUser = async (id: string): Promise<AxiosResponse<Users.User>> =>
  await axios.delete(`${API_BASE_URL}/users/${id}`);

export const updateUser = async ({
  id,
  data,
 }: Users.UpdateParam): Promise<AxiosResponse<Users.User>> =>
  await axios.put(`${API_BASE_URL}/users/${id}`, data);
