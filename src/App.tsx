import './assets/app.global.css';

import * as React from 'react';

import { UserList } from 'components/Users/List';
import { Container, Row, Col } from 'react-bootstrap';
import { CreateUser } from 'components/Users/Create';
import { RandomUser } from 'components/Users/RandomUser';

// tslint:disable-next-line:variable-name
export const App: React.FC = () => (
  <Container>
    <hr/>
    <Row>
      <RandomUser/>
    </Row>
    <hr/>
    <Row>
      <Col md={6}>
        <UserList/>
      </Col>
      <Col md={6}>
        <CreateUser/>
      </Col>
    </Row>
  </Container>
);
