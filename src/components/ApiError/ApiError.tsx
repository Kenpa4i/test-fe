import * as React from 'react';
import { AxiosError } from 'axios';
import { Alert } from 'react-bootstrap';

interface Props {
  error: AxiosError;
}
// tslint:disable-next-line:variable-name
export const ApiError: React.FC<Props> = ({ error }) => error && (
  <Alert variant="danger">
    {error.response.data}
  </Alert>
);
