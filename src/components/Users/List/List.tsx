import * as React from 'react';
import { Users } from 'store/users/types';
import { RootState } from 'store/types';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { getUserListAsync } from 'store/users/users.actions';
import { ListGroup, Spinner } from 'react-bootstrap';
import { ListItem } from '../ListItem';
import { ApiError } from 'components/ApiError';

type Props = MapStateProps & MapDispatchProps;

// tslint:disable-next-line:variable-name
export const List: React.FC<Props> = ({
  data,
  error,
  loading,
  getList,
}) => {
  React.useEffect(() => {
    getList();
  },              []);

  const renderLoading = () => loading && (
    <Spinner animation="border" />
  );
  const renderData = () => !!data.length && data.map(({ details: { id } }) => {
    return (
      <ListItem id={id} key={id}/>
    );
  });
  const renderEmptyList = () => !data.length && !loading && (
    <>
      List is empty ☹
    </>
  );
  return (
    <ListGroup>
      <ApiError error={error}/>
      {renderLoading()}
      {renderData()}
      {renderEmptyList()}
    </ListGroup>
  );
};

type MapStateProps = Users.State.List;
const mapStateToProps = ({
  users: {
    list,
  },
 }: RootState): MapStateProps => ({
   ...list,
 });

interface MapDispatchProps {
  getList: () => void;
}

const mapDispatchToProps = (dispatch: Dispatch): MapDispatchProps => ({
  getList: () => dispatch(getUserListAsync.request()),
});
export default connect(mapStateToProps, mapDispatchToProps)(List);
