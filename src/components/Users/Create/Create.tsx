import * as React from 'react';
import { Users } from 'store/users/types';
import { Dispatch } from 'redux';
import { postUserAsync } from 'store/users/users.actions';
import { connect } from 'react-redux';
import { Form, Button, Card, Spinner } from 'react-bootstrap';
import { Formik, Field, Form as FormikForm } from 'formik';
import { RequestState, RootState } from 'store/types';
import * as yup from 'yup';
import { ApiError } from 'components/ApiError';

const initValues: Users.UserData = {
  birthDate: '',
  email: '',
  name: '',
};
const schema = yup.object().shape({
  email: yup.string().email().required('Email required'),
  name: yup.string().required('Name required'),
  birthDate: yup.date().max(
    new Date(), 'Date of birth in feature ?').required('BirthDate required'),
});
type Props = MapStateProps & MapDispatchProps;

// tslint:disable-next-line:variable-name
export const Create: React.FC<Props> = ({
  createUser,
  error,
  loading,
}) => {
  return (
    <Formik
      initialValues={initValues}
      onSubmit={createUser}
      validationSchema={schema}
    >
      {form => (
        <Card>
          <Card.Header>
            Create user
          </Card.Header>
          <Card.Body>
          <FormikForm>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Field as={Form.Control} name="email" type="email" placeholder="Enter email"/>
              <Form.Text className="text-muted">
                {form.errors.email}
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicName">
              <Form.Label>Name</Form.Label>
              <Field as={Form.Control} name="name" type="text" placeholder="Enter name"/>
              <Form.Text className="text-muted">
                {form.errors.name}
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicBirthDate">
              <Form.Label>Birth date</Form.Label>
              <Field as={Form.Control} name="birthDate" type="date" placeholder="Enter Birth date"/>
              <Form.Text className="text-muted">
                {form.errors.birthDate}
              </Form.Text>
            </Form.Group>
            <ApiError error={error}/>
          </FormikForm>
          </Card.Body>
          <Card.Footer>
            <Button variant="primary" type="submit" onClick={form.submitForm}>
              {loading
                ? (<Spinner size="sm" animation="border"/>)
                : 'Submit'}
            </Button>
          </Card.Footer>
        </Card>
      )}
    </Formik>
  );
};

type MapStateProps = RequestState;
const mapStateToProps = ({ users: { create } }: RootState): MapStateProps => create;

interface MapDispatchProps {
  createUser: (data: Users.UserData) => void;
}

const mapDispatchToProps = (dispatch: Dispatch): MapDispatchProps => ({
  createUser: data => dispatch(postUserAsync.request(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Create);
