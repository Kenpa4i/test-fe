import * as React from 'react';
import { ListGroup, Spinner, Button } from 'react-bootstrap';
import { Users } from 'store/users/types';
import { RootState } from 'store/types';
import { isUserId } from 'store/users/users.selectors';
import { Dispatch } from 'redux';
import { deleteUserAsync, updateUserAsync } from 'store/users/users.actions';
import { connect } from 'react-redux';
import { UpdateUser } from '../Update';
import { ApiError } from 'components/ApiError';

interface OwnProps {
  id: string;
}

type Props = OwnProps & MapStateProps & MapDispatchProps;

// tslint:disable-next-line:variable-name
export const ListItem: React.FC<Props> = ({
  id,
  details: {
    email,
  },
  deleteUser,
  statuses: {
    delete: {
      loading,
      error,
    },
  },
}) => {
  const [showUpdate, setShowUpdate] = React.useState(false);
  return (
    <ListGroup.Item>
      <div>
        {email}
        <Button
          variant="danger"
          onClick={deleteUser}>
            {loading
                ? (<Spinner size="sm" animation="border"/>)
                : 'Remove'}
          </Button>
        <Button variant="info" onClick={() => setShowUpdate(!showUpdate)}>Edit</Button>
        <ApiError error={error}/>
      </div>
      {showUpdate && (
        <div>
          <UpdateUser id={id}/>
        </div>
      )}
    </ListGroup.Item>
  );
};

type MapStateProps = Users.StatefullUser;

const mapStateToProps = ({
  users: {
    list: {
      data,
    },
  },
}: RootState,            {
  id,
}: OwnProps): MapStateProps => data.find(isUserId(id));

interface MapDispatchProps {
  deleteUser: () => void;
}

const mapDispatchToProps = (
  dispatch: Dispatch,
  { id }: OwnProps,
): MapDispatchProps => ({
  deleteUser: () => dispatch(deleteUserAsync.request(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ListItem);
