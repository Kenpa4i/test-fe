import * as React from 'react';
import { Users } from 'store/users/types';
import { Dispatch } from 'redux';
import { updateUserAsync } from 'store/users/users.actions';
import { connect } from 'react-redux';
import { Form, Button, Card, Spinner } from 'react-bootstrap';
import { Formik, Field, Form as FormikForm } from 'formik';
import { RequestState, RootState } from 'store/types';
import * as yup from 'yup';
import { isUserId } from 'store/users/users.selectors';
import { ApiError } from 'components/ApiError';
import { format } from 'date-fns';

const schema = yup.object().shape({
  email: yup.string().email().required('Email required'),
  name: yup.string().required('Name required'),
  birthDate: yup.date().max(
    new Date(), 'Date of birth in feature ?').required('BirthDate required'),
});

interface OwnProps {
  id: string;
}
type Props = OwnProps & MapStateProps & MapDispatchProps;

// tslint:disable-next-line:variable-name
export const Update: React.FC<Props> = ({
  updateUser,
  asyncState: {
    error,
    loading,
  },
  details,
}) => {
  const { birthDate, email, name }: Users.UserData = details;
  const getDateString = (date: string): string =>
    format(new Date(date), 'yyyy-MM-dd');

  return (
    <Formik
      initialValues={{ birthDate, email, name }}
      onSubmit={updateUser}
      validationSchema={schema}
    >
      {form => (
        <Card>
          <Card.Header>
            Update user
          </Card.Header>
          <Card.Body>
          <FormikForm>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Field as={Form.Control} name="email" type="email" placeholder="Enter email"/>
              <Form.Text className="text-muted">
                {form.errors.email}
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicName">
              <Form.Label>Name</Form.Label>
              <Field as={Form.Control} name="name" type="text" placeholder="Enter name"/>
              <Form.Text className="text-muted">
                {form.errors.name}
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicBirthDate">
              <Form.Label>Birth date</Form.Label>
              <Form.Control
                type="date"
                onChange={(event: React.FormEvent<HTMLInputElement>) => {
                  form.setFieldValue('birthDate', getDateString(event.currentTarget.value));
                }}
                value={form.values.birthDate  !== '' ? getDateString(form.values.birthDate) : ''}
              />
              <Form.Text className="text-muted">
                {form.errors.birthDate}
              </Form.Text>
            </Form.Group>
            <ApiError error={error}/>
          </FormikForm>
          </Card.Body>
          <Card.Footer>
            <Button variant="primary" type="submit" onClick={form.submitForm}>
              {loading
                ? (<Spinner size="sm" animation="border"/>)
                : 'Submit'}
            </Button>
          </Card.Footer>
        </Card>
      )}
    </Formik>
  );
};

interface MapStateProps {
  details: Users.UserData;
  asyncState: RequestState;
}
const mapStateToProps = (
  { users: { list: { data } } }: RootState,
  { id }: OwnProps,
  ): MapStateProps => {
  const { details, statuses: { update } } = data.find(isUserId(id));
  return {
    details,
    asyncState: update,
  };
};

interface MapDispatchProps {
  updateUser: (data: Users.UserData) => void;
}

const mapDispatchToProps = (dispatch: Dispatch, { id }: OwnProps): MapDispatchProps => ({
  updateUser: data => dispatch(updateUserAsync.request({ id, data })),
});

export default connect(mapStateToProps, mapDispatchToProps)(Update);
