import * as React from 'react';
import { Users } from 'store/users/types';
import { RootState } from 'store/types';
import { connect } from 'react-redux';
import { Button, Col, Row } from 'react-bootstrap';

type Props = MapStateProps;
// tslint:disable-next-line:variable-name
export const RandomUser: React.FC<Props> = ({
  list,
}) => {
  const [lastUser, setLastUser] = React.useState<Users.StatefullUser>(null);

  const getRandomUser = <T extends any>(array: T[]):T =>
   array[Math.floor(Math.random() * array.length)];

  const generateNotRepeatedUser = () => {
    const rand = getRandomUser(list);
    if (!lastUser || list.length === 1 || rand.details.id !== lastUser.details.id) {
      setLastUser(rand);
      return false;
    }
    generateNotRepeatedUser();
  };

  const onRandomClick = () => {
    generateNotRepeatedUser();
  };

  const renderUser = () => !!lastUser  && lastUser.details.email;
  return (
    <>
      <Col>
        <div>
        Last user email: {renderUser()}
        </div>
        {list.length <= 1 && 'Not enough user to get random not repeated user'}
      </Col>
      <Col>
        <Button
          disabled={!list.length}
          onClick={onRandomClick}>
            Randomise user
        </Button>
      </Col>
    </>
  );
};

interface MapStateProps {
  list: Users.StatefullUser[];
}

const mapStateToProps = ({ users: { list: { data } } }: RootState): MapStateProps => ({
  list: data,
});

export default connect(mapStateToProps)(RandomUser);
