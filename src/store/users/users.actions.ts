import { createAsyncAction } from 'typesafe-actions';
import { Users } from './types';
import { AxiosError } from 'axios';

export const getUserListAsync = createAsyncAction(
  'USERS | GET_USER_LIST_REQ',
  'USERS | GET_USER_LIST_RES',
  'USERS | GET_USER_LIST_ERR',
)<void, Users.User[], AxiosError>();

export const postUserAsync = createAsyncAction(
  'USERS | POST_USER_REQ',
  'USERS | POST_USER_RES',
  'USERS | POST_USER_ERR',
)<Users.UserData, Users.User, AxiosError>();

export const deleteUserAsync = createAsyncAction(
  'USERS | DELETE_USER_REQ',
  'USERS | DELETE_USER_RES',
  'USERS | DELETE_USER_ERR',
)<string, string, Users.ErrorParam>();

export const updateUserAsync = createAsyncAction(
  'USERS | UPDATE_USER_REQ',
  'USERS | UPDATE_USER_RES',
  'USERS | UPDATE_USER_ERR',
)<Users.UpdateParam, Users.User, Users.ErrorParam>();
