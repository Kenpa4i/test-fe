import { Users } from './types';
import { defaultRequestState } from 'store/utils';
import { AxiosError } from 'axios';

export const makeStatefullUser = (user: Users.User): Users.StatefullUser => ({
  details: user,
  statuses: {
    delete: defaultRequestState,
    update: defaultRequestState,
  },
});

const updateUser = (
  id: string,
  user: Users.StatefullUser,
  upd: Users.StatefullUser,
  ) => user.details.id === id ? upd : user;

export const setUserStatusReq = (id: string, status: Users.StatusesKeys) => (
  user: Users.StatefullUser,
): Users.StatefullUser => updateUser(id, user, {
  ...user,
  statuses: {
    ...user.statuses,
    [status]: {
      loading: true,
      error: null,
    },
  },
});

export const updateRes = (details: Users.User, status: Users.StatusesKeys) => (
  user: Users.StatefullUser,
): Users.StatefullUser => updateUser(details.id, user, {
  details,
  statuses: {
    ...user.statuses,
    [status]: {
      loading: false,
      error: null,
    },
  },
});

export const setUserStatusErr = (
  id: string,
  error: AxiosError,
  status: Users.StatusesKeys,
) => (
  user: Users.StatefullUser,
): Users.StatefullUser => updateUser(id, user, {
  ...user,
  statuses: {
    ...user.statuses,
    [status]: {
      error,
      loading: false,
    },
  },
});

export const isUserId = (id: string) => (
  user: Users.StatefullUser,
): boolean => user.details.id === id;

export const notUserId = (id: string) => (
  user: Users.StatefullUser,
): boolean => user.details.id !== id;
