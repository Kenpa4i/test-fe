import { RequestState } from 'store/types';
import { AxiosError } from 'axios';

export namespace Users {
  export interface User extends UserData{
    createDate: string;
    updateDate: string;
    id: string;
  }
  export interface UpdateParam {
    id: string;
    data: UserData;
  }
  export interface ErrorParam {
    id: string;
    error: AxiosError;
  }
  export interface UserData {
    email: string;
    name: string;
    birthDate: string;
  }
  export enum StatusesKeys {
    delete = 'delete',
    update = 'update',
  }
  type UserStatuses = {
    [key in StatusesKeys]: RequestState;
  };

  export interface StatefullUser {
    details: User;
    statuses: UserStatuses;
  }
  export namespace State {
    export interface List extends RequestState {
      data: StatefullUser[];
    }
    export type Create = RequestState;
  }
}
