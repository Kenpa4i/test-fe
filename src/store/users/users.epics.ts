import { GenEpic } from 'store/types';
import { filter, flatMap, catchError, map } from 'rxjs/operators';
import { isActionOf } from 'typesafe-actions';
import { from, of } from 'rxjs';
import { AxiosError } from 'axios';
import {
  getUserListAsync,
  postUserAsync,
  deleteUserAsync,
  updateUserAsync,
} from './users.actions';

export const toResposeToGetUserListReq: GenEpic = (action$, state$, { api }) =>
action$.pipe(
  filter(isActionOf(getUserListAsync.request)),
  flatMap(() =>
    from(api.users.getUserList()).pipe(
      map(({ data }) => getUserListAsync.success(data)),
      catchError((err: AxiosError) => of(getUserListAsync.failure(err))),
    ),
  ),
);

export const toResposeToPostUserReq: GenEpic = (action$, state$, { api }) =>
action$.pipe(
  filter(isActionOf(postUserAsync.request)),
  flatMap(({ payload }) =>
    from(api.users.postUser(payload)).pipe(
      map(({ data }) => postUserAsync.success(data)),
      catchError((err: AxiosError) => of(postUserAsync.failure(err))),
    ),
  ),
);

export const toResposeToDeleteUserReq: GenEpic = (action$, state$, { api }) =>
action$.pipe(
  filter(isActionOf(deleteUserAsync.request)),
  flatMap(({ payload: id }) =>
    from(api.users.deleteUser(id)).pipe(
      map(() => deleteUserAsync.success(id)),
      catchError((error: AxiosError) => of(deleteUserAsync.failure({ id, error }))),
    ),
  ),
);

export const toResposeToUpdateUserReq: GenEpic = (action$, state$, { api }) =>
action$.pipe(
  filter(isActionOf(updateUserAsync.request)),
  flatMap(({ payload }) =>
    from(api.users.updateUser(payload)).pipe(
      map(({ data }) => updateUserAsync.success(data)),
      catchError((error: AxiosError) => of(updateUserAsync.failure({
        error,
        id: payload.id,
      }))),
    ),
  ),
);
