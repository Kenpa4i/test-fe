import { createReducer } from 'typesafe-actions';
import { combineReducers } from 'redux';
import { Users } from './types';
import { RootAction } from 'store/types';
import {
  getUserListAsync,
  postUserAsync,
  deleteUserAsync,
  updateUserAsync,
} from './users.actions';
import { defaultRequestState } from 'store/utils';
import {
  makeStatefullUser,
  notUserId,
  setUserStatusReq,
  setUserStatusErr,
  updateRes,
} from './users.selectors';

const initListState: Users.State.List = {
  ...defaultRequestState,
  data: [],
};

const list = createReducer<Users.State.List, RootAction>(initListState)
.handleAction(getUserListAsync.request, state => ({
  ...state,
  loading: true,
  error: null,
}))
.handleAction(getUserListAsync.success, (state, { payload }) => ({
  ...state,
  data: payload.map(makeStatefullUser),
  loading: false,
}))
.handleAction(getUserListAsync.failure, (state, { payload }) => ({
  ...state,
  error: payload,
  loading: false,
}))
.handleAction(postUserAsync.success, (state, { payload }) => ({
  ...state,
  data: state.data.concat(makeStatefullUser(payload)),
}))
.handleAction(deleteUserAsync.request, (state, { payload }) => ({
  ...state,
  data: state.data.map(setUserStatusReq(payload, Users.StatusesKeys.delete)),
}))
.handleAction(deleteUserAsync.success, (state, { payload: id }) => ({
  ...state,
  data: state.data.filter(notUserId(id)),
}))
.handleAction(deleteUserAsync.failure, (state, { payload: { id, error } }) => ({
  ...state,
  data: state.data.map(setUserStatusErr(id, error, Users.StatusesKeys.delete)),
}))
.handleAction(updateUserAsync.request, (state, { payload: { id } }) => ({
  ...state,
  data: state.data.map(setUserStatusReq(id, Users.StatusesKeys.update)),
}))
.handleAction(updateUserAsync.success, (state, { payload }) => ({
  ...state,
  data: state.data.map(updateRes(payload, Users.StatusesKeys.update)),
}))
.handleAction(updateUserAsync.failure, (state, { payload: { id, error } }) => ({
  ...state,
  data: state.data.map(setUserStatusErr(id, error, Users.StatusesKeys.update)),
}))
;

const create = createReducer<Users.State.Create, RootAction>(defaultRequestState)
.handleAction(postUserAsync.request, () => ({
  loading: true,
  error: null,
}))
.handleAction(postUserAsync.success, state => ({
  ...state,
  loading: false,
}))
.handleAction(postUserAsync.failure, (state, { payload }) => ({
  error: payload,
  loading: false,
}));

export const usersReducer = combineReducers({
  list,
  create,
});
