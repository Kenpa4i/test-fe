import { Epic } from 'redux-observable';
import { ActionType, StateType } from 'typesafe-actions';
import { AxiosError } from 'axios';

export type RootAction = ActionType<typeof import('./root-action').default>;
export type Store = StateType<ReturnType<typeof import('./index').default>>;
export type RootState = StateType<typeof import('./root-reducer').default>;
export type Services = typeof import('./../services/index').default;
export type GenEpic = Epic<RootAction, RootAction, RootState, Services>;

export interface RequestState {
  loading: boolean;
  error: AxiosError;
}
