import { compose } from 'redux';
import { RequestState } from './types';

export const composeEnhancers =
  (process.env.NODE_ENV === 'development' &&
    window &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

export  const defaultRequestState: RequestState = {
  error: null,
  loading: false,
};
