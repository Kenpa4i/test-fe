import * as usersActions from './users/users.actions';

export default {
  users: usersActions,
};
