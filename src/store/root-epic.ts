import { combineEpics } from 'redux-observable';

import * as userEpics from './users/users.epics';

export default combineEpics(
  ...Object.values(userEpics),
);
