# Test project for FE

## Instalation

Dependency:
- [API project](https://gitlab.com/Kenpa4i/test-be)

```shell
$ yarn install
```

## Development

```shell
$ yarn start:dev
```

## Build

```shell
$ yarn build
```
